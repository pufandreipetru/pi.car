#!/bin/sh
# Start process in debug mode: sudo java -agentlib:jdwp=transport=dt_socket,address=8080,server=y,suspend=y -jar target/pi_car-rc.jar
# Start process: sudo java -jar target/pi_car-rc.jar

mvn clean package

cd target

mv pi_car-jar-with-dependencies.jar pi_car-rc.jar

sshpass -p "dnstuff" scp pi_car-rc.jar pi@192.168.4.1:/home/pi/pi_car

cd ..