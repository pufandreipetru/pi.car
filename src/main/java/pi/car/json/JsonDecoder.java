package pi.car.json;


import pi.car.model.NetworkDataPackage;

import java.io.IOException;

/**
 * Json decoder.
 * @see ObjectMapperUtils
 */
public class JsonDecoder {

    /**
     * Decodes a json string to a DataPackage object.
     * @param jsonString
     * @return
     * @throws IOException
     */
    public NetworkDataPackage decode(String jsonString) throws IOException {
        return ObjectMapperUtils.getObjectMaopper().fromJson(jsonString, NetworkDataPackage.class);
    }

    /**
     * Encodes a DataPackage object to json string then to byte array.
     * @param dataPackage
     * @return
     * @throws IOException
     */
    public String encode(NetworkDataPackage dataPackage) throws IOException {
        return ObjectMapperUtils.getObjectMaopper().toJson(dataPackage);
    }
}
