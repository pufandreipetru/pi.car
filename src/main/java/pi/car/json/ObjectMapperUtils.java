package pi.car.json;

import com.google.gson.Gson;

/**
 * ObjectMapperUtils, singleton ObjectMapper instance.
 */
public class ObjectMapperUtils {
    private static Gson objectMapper;

    /**
     * Retrieves a reference to the ObjectMapper instance.
     *
     * @return
     */
    public static Gson getObjectMaopper() {
        if (objectMapper == null) {
            objectMapper = new Gson();
        }
        return objectMapper;
    }

}
