package pi.car.controller;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.GpioPinPwmOutput;
import com.pi4j.io.gpio.Pin;
import pi.car.model.NetworkDataPackage;

/**
 * The controller class for L298N chip.
 */
public class L298NController {
    private GpioPinDigitalOutput inputLeftForward;
    private GpioPinDigitalOutput inputLeftBackward;
    private GpioPinPwmOutput pwmLeft;

    private GpioPinDigitalOutput inputRightForward;
    private GpioPinDigitalOutput inputRightBackward;
    private GpioPinPwmOutput pwmRight;

    /**
     * Creates the new objec of L298NController type.
     * @param forwardLeftPin - GPIO pin for controlling the left forward motion
     * @param backwardLeftPin - GPIO pin for controlling the left backwards motion
     * @param pwmLeftPin - pulse with modulation value for the left side
     * @param forwardRightPin - GPIO pin for controlling the right forward motion
     * @param backwardRightPin GPIO pin for controlling the right backwards motion
     * @param pwmRightPin - pulse with modulation value for the right side
     */
    public L298NController(Pin forwardLeftPin, Pin backwardLeftPin,
                           Pin pwmLeftPin, Pin forwardRightPin,
                           Pin backwardRightPin, Pin pwmRightPin) {
        final GpioController gpio = GpioFactory.getInstance();

        this.inputLeftForward = gpio.provisionDigitalOutputPin(forwardLeftPin);
        this.inputLeftBackward = gpio.provisionDigitalOutputPin(backwardLeftPin);
        this.pwmLeft = gpio.provisionSoftPwmOutputPin(pwmLeftPin);
        this.pwmLeft.setPwm(0);

        this.inputRightForward = gpio.provisionDigitalOutputPin(forwardRightPin);
        this.inputRightBackward = gpio.provisionDigitalOutputPin(backwardRightPin);
        this.pwmRight = gpio.provisionSoftPwmOutputPin(pwmRightPin);
        this.pwmRight.setPwm(0);
    }

    /**
     * Sets the PWM value for both sides of the car
     * @param pwm
     */
    public void setPwm(int pwm) {
        this.pwmLeft.setPwm(pwm);
        this.pwmRight.setPwm(pwm);
    }

    /**
     * Sets the PWM value for the right side of the car
     * @param pwmLeft
     */
    public void setPwmLeft(int pwmLeft) {
        this.pwmLeft.setPwm(pwmLeft);
    }

    /**
     * Sets the PWM value for the right side of the car
     * @param pwmRight
     */
    public void setPwmRight(int pwmRight) {
        this.pwmRight.setPwm(pwmRight);
    }

    /**
     * Sets the GPIO commands for going forward on the left side of
     * the car
     */
    public void goForwardLeft() {
        this.inputLeftForward.high();
        this.inputLeftBackward.low();
    }
    /**
     * Sets the GPIO commands for going forward on the right side of
     * the car
     */
    public void goForwardRight() {
        this.inputRightForward.high();
        this.inputRightBackward.low();
    }

    /**
     * Sets the GPIO commands for going backwards on the left side of
     * the car
     */
    public void goBackwardLeft(){
        this.inputLeftForward.low();
        this.inputLeftBackward.high();
    }

    /**
     * Sets the GPIO commands for going backwards on the right side of
     * the car
     */
    public void goBackwardRight() {
        this.inputRightForward.low();
        this.inputRightBackward.high();
    }

    /**
     * Sets the GPIO commands for going forwards
     */
    public void goForward() {
        goForwardLeft();
        goForwardRight();
    }

    /**
     * Sets the GPIO commands for going backwards
     */
    public void goBackward() {
        goBackwardLeft();
        goBackwardRight();
    }

    /**
     * Stops all motion commands
     */
    public void stop() {
        this.inputLeftForward.low();
        this.inputLeftBackward.low();
        this.inputRightForward.low();
        this.inputRightBackward.low();
    }

    /**
     * Translates a NetworkDataPackage object into GPIO comands.
     * @param inputDataPackage - network data package object received
     * @see NetworkDataPackage
     */
    public synchronized void sendDataPackage(NetworkDataPackage inputDataPackage) {
            // up = true
            if(inputDataPackage.isUp() && !inputDataPackage.isDown() && !inputDataPackage.isLeft() && !inputDataPackage.isRight()) {
                setPwm(100);
                goForward();
            }
            // up = true and down = false and left = false and right = true
            if(inputDataPackage.isUp() && !inputDataPackage.isDown() && !inputDataPackage.isLeft() && inputDataPackage.isRight()) {
                setPwm(100);
                setPwmRight(5);
                goForward();
            }
            // up = true and down = false and left = true and right = false
            if(inputDataPackage.isUp() && !inputDataPackage.isDown() && inputDataPackage.isLeft() && !inputDataPackage.isRight()) {
                setPwm(100);
                setPwmLeft(5);
                goForward();
            }
            // up = false and down = true and left = false and right = false
            if(!inputDataPackage.isUp() && inputDataPackage.isDown() && !inputDataPackage.isLeft() && !inputDataPackage.isRight()) {
                setPwm(100);
                goBackward();
            }
            // up = false and down = true and left = false and right = true
            if(!inputDataPackage.isUp() && inputDataPackage.isDown() && !inputDataPackage.isLeft() && inputDataPackage.isRight()) {
                setPwm(100);
                setPwmRight(5);
                goBackward();
            }
            // up = false and down = true and left = true and right = false
            if(!inputDataPackage.isUp() && inputDataPackage.isDown() && inputDataPackage.isLeft() && !inputDataPackage.isRight()) {
                setPwm(100);
                setPwmLeft(5);
                goBackward();
            }
            // up = false and down = false and left = true and right = false
            if(!inputDataPackage.isUp() && !inputDataPackage.isDown() && inputDataPackage.isLeft() && !inputDataPackage.isRight()) {
                setPwm(80);
                goForwardRight();
                goBackwardLeft();
            }
            // up = false and down = false and left = false and right = true
            if(!inputDataPackage.isUp() && !inputDataPackage.isDown() && !inputDataPackage.isLeft() && inputDataPackage.isRight()) {
                setPwm(80);
                goForwardLeft();
                goBackwardRight();
            }
            // up = false and down = false and left = false and right = false
            if(!inputDataPackage.isUp() && !inputDataPackage.isDown() && !inputDataPackage.isLeft() && !inputDataPackage.isRight()) {
                setPwm(0);
                stop();
            }
    }
}
