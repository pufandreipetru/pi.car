package pi.car.main;

import org.apache.log4j.LogManager;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import pi.car.socket.server.ServerSynchronizerRunnable;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Main class for the pi_car
 */
public class PiCarMain {
    private static final Logger LOG = Logger.getLogger(PiCarMain.class);
    private ExecutorService executorService;

    /**
     * Constructs a new PiCarMain object consisting of a simple HTTP server
     * which will listen for command received from: @see /resource/www/index.html
     * The command line will be ad http://ip:9090/index.html
     */
    public PiCarMain() {
        this.executorService = Executors.newSingleThreadExecutor();
        this.executorService.submit(new ServerSynchronizerRunnable(9090));
    }

    /**
     * Main method of the server waiting forever after starting the server thread.
     *
     * @param args
     */
    public static void main(String... args) {

        new PiCarMain();

        while(1==1);
    }
}
