package pi.car.model;

import org.apache.log4j.Logger;

import java.util.List;

/**
 * Model for the headers.
 * NOTE:
 * For performance reasons the headers were kept in raw format and
 * converted for retrieving the value on demand only.
 *
 * Example:
 *
 * POST / HTTP/1.1
 * Host: 192.168.4.1:9090
 * Connection: keep-alive
 * Content-Length: 51
 * Origin: null
 * User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36
 * Content-Type: text/plain;charset=UTF-8
 * Accept: text/html
 * Accept-Encoding:gzip,deflate
 * Accept-Language:en-US,en;q=0.9
 */
public class Headers {
    private static final int METHOD_INDEX = 0;
    private static final int PATH_INDEX = 1;
    private static final String DELIMITER = ":";
    private static final int HEADER_VALUE_INDEX = 1;
    private static final int VALID_HEADER_KEY_VALUE_LENGTH = 2;
    private static final String EMPTY = "";
    private static final Logger LOG = Logger.getLogger(Headers.class);

    private String [] protocolValues;
    private List<String> headerLines;

    /**
     * Creates a new Headers object
     * @param headerLines - string lines containing the headers
     */
    public Headers(List<String> headerLines) {
        this.headerLines = headerLines;
    }

    /**
     * Gets a header value as string by key if exists.
     * @param headerKey - name of the header
     * @return - value or empty string if it does not exist
     */
    public String getHeader(String headerKey) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Retrieving the header " + headerKey);
        }
        for(String header : headerLines) {
            if(header.contains(headerKey)) {
                String[] headerKeyValuePair = header.split(DELIMITER);
                if(headerKeyValuePair.length == VALID_HEADER_KEY_VALUE_LENGTH) {
                    return headerKeyValuePair[HEADER_VALUE_INDEX];
                }
            }
        }
        return "";
    }

    /**
     * Retrives the HTTP method of the request
     *
     * @return - http method or empty string if it does not exist
     */
    public String getMethod() {
        calculateProtocolValues();
        if(protocolValues.length > 0) {
            return protocolValues[METHOD_INDEX];
        }
        return EMPTY;
    }

    private void calculateProtocolValues() {
        if (protocolValues == null) {
            if(headerLines !=null && headerLines.size() > 0)
            protocolValues = headerLines.get(0).split(" ");
        }
    }

    /**
     * Retries the path requested
     *
     * @return path or empty string if it does not exist
     */
    public String getPath() {
        calculateProtocolValues();
        if(protocolValues.length > 0) {
            return protocolValues[PATH_INDEX];
        }
        return EMPTY;
    }
}
