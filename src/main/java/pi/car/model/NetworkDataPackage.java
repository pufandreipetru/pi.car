package pi.car.model;

/**
 * Package containing the commands.
 */
public class NetworkDataPackage {
    private boolean up;
    private boolean down;
    private boolean left;
    private boolean right;

    /**
     * Returns true if the up arrow was pressed
     * @return
     */
    public boolean isUp() {
        return up;
    }

    /**
     * Sets the value to true if the up arrow was pressed or not
     * @param up
     */
    public void setUp(boolean up) {
        this.up = up;
    }

    /**
     * Returns true if the down arrow was pressed
     * @return
     */
    public boolean isDown() {
        return down;
    }

    /**
     * Sets the value to true if the down arrow was pressed or not
     * @param down
     */
    public void setDown(boolean down) {
        this.down = down;
    }

    /**
     * Returns true if the left arrow was pressed
     * @return
     */
    public boolean isLeft() {
        return left;
    }

    /**
     * Sets the value to true if the left arrow was pressed or not
     * @param left
     */
    public void setLeft(boolean left) {
        this.left = left;
    }

    /**
     * Returns true if the right arrow was pressed
     * @return
     */
    public boolean isRight() {
        return right;
    }
    /**
     * Sets the value to true if the right arrow was pressed or not
     * @param right
     */
    public void setRight(boolean right) {
        this.right = right;
    }
}
