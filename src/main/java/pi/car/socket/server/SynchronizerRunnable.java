package pi.car.socket.server;

import pi.car.controller.L298NController;
import pi.car.json.JsonDecoder;

/**
 * Runnable able to synchronize and stop.
 */
public interface SynchronizerRunnable extends StopableRunnable {
    /**
     * Register the synchronizable to synchronize or follow.
     * @param l298NController
     */
    void register(L298NController l298NController);

    /**
     * Register the jsonDecoder used to encode/decode the dataPackage
     * @param jsonDecoder
     */
    void register(JsonDecoder jsonDecoder);
}
