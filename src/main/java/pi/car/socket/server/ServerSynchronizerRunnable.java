package pi.car.socket.server;

import com.pi4j.io.gpio.RaspiPin;
import pi.car.controller.L298NController;
import pi.car.json.JsonDecoder;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.apache.log4j.Logger;

/**
 * Basic HTTP server using websocket server that will respond up to 15 clients.
 */
public class ServerSynchronizerRunnable implements SynchronizerRunnable {

    private static final int THREAD_COUNT = 15;
    private static Logger LOG = Logger.getLogger(ServerSynchronizerRunnable.class.getName());

    private int          serverPort   = 8080;
    private ServerSocket serverSocket = null;
    private boolean      isStopped    = false;
    private L298NController l298NController;
    private ExecutorService executorService;
    private JsonDecoder jsonDecoder;

    /**
     * Creates a new ServerRunnable object.
     * @param port
     */
    public ServerSynchronizerRunnable(int port) {
        this.serverPort = port;
        this.executorService = Executors.newFixedThreadPool(THREAD_COUNT);
        this.jsonDecoder = new JsonDecoder();
        this.l298NController = new L298NController(RaspiPin.GPIO_05,
                RaspiPin.GPIO_04, RaspiPin.GPIO_06, RaspiPin.GPIO_03,
                RaspiPin.GPIO_00, RaspiPin.GPIO_02);
    }

    public void run() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Starting the server on port: " + serverPort);
        }
        try {
            openServerSocket();
            while (!isStopped()) {
                try {
                    Socket clientSocket = this.serverSocket.accept();
                    WorkerRunnable workerRunnable = new WorkerRunnable(clientSocket, l298NController, jsonDecoder);
                    this.executorService.submit(workerRunnable);
                }
                catch (IOException e) {
                    LOG.error(e.getMessage(), e);
                    throw new RuntimeException("Error accepting client connection", e);
                } catch (Exception e) {
                    LOG.error(e.getMessage(), e);
                }
            }
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
        LOG.info("Master Stopped.");
    }

    /**
     * Checks if the master thread is stopped
     *
     * @return boolean
     */
    private synchronized boolean isStopped() {
        return this.isStopped;
    }

    /**
     * Stops the master thread
     */
    @Override
    public synchronized void stop(){
        this.isStopped = true;
        this.executorService.shutdownNow();
        try {
            this.serverSocket.close();
        } catch (IOException e) {
            throw new RuntimeException("Error closing server", e);
        }
    }

    private void openServerSocket() {
        try {
            this.serverSocket = new ServerSocket(this.serverPort);
        } catch (IOException e) {
            throw new RuntimeException("Cannot open port " + this.serverPort, e);
        }
    }

    /**
     * Registers a syncronizable object to control.
     *
     * @param l298NController
     */
    @Override
    public void register(L298NController l298NController) {
        this.l298NController = l298NController;
    }

    /**
     * Registers the JsonDecoder to use for serializing the requests.
     *
     * @param jsonDecoder
     */
    @Override
    public void register(JsonDecoder jsonDecoder) {
        this.jsonDecoder = jsonDecoder;
    }
}