package pi.car.socket.server.handler;

import pi.car.model.Headers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import org.apache.log4j.Logger;
/**
 * Base HTTP request handler
 */
public class BaseRequestHandler implements RequestHandler {
    private static final Logger LOG = Logger.getLogger(BaseRequestHandler.class.getName());
    protected String requestBody;
    protected static final String HTTP_STATUS_OK = "200 OK";
    protected static final String HTTP_STATUS_NOT_FOUND = "404 Not Found";

    @Override
    public String handle(Headers headers, BufferedReader in, PrintWriter out) throws IOException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Handling UNKNOWN request");
        }
        return null;
    }


    protected void readRequestBody(Headers headers, BufferedReader in) throws IOException {
        Integer contentLength = Integer.valueOf(
                headers.getHeader("Content-Length").trim().length() > 0
                        ? headers.getHeader("Content-Length").trim()
                        : "0" );

        if(contentLength != null) {
            LOG.info("Content length: " + contentLength);
            char[] content = new char[contentLength];
            in.read(content);
            if (LOG.isDebugEnabled()) {
                LOG.debug("BODY: " + content);
            }
            this.requestBody = new String(content);
        }
    }

    protected void writeResponse(PrintWriter out, String status, String data) {
        out.println("HTTP/1.1 " + status);
        out.println("Server: Raspberrypi minimal : 1.0");
        out.println("Date: " + new Date());
        out.println("Access-Control-Allow-Origin: *");
        out.println("Content-type: text/html");
        out.println("Content-length: "+ data.getBytes().length);
        out.println();
        out.println(data);
        out.flush();
    }

}
