package pi.car.socket.server.handler;

import pi.car.model.Headers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

public interface RequestHandler {
     /**
      * Handles a http request.
      *
      * @param headers - http headers
      * @param in - input stream wrapped as a BufferedReader object
      * @param out - output stream wrapped as a PrintWriter object
      * @return
      * @throws IOException
      */
     String handle(Headers headers, BufferedReader in, PrintWriter out) throws IOException;
}
