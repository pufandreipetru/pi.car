package pi.car.socket.server.handler;
import pi.car.model.Headers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import org.apache.log4j.Logger;
/**
 * Basic request handler for GET method
 */
public class GetRequestHandler extends BaseRequestHandler {
    private static final Logger LOG = Logger.getLogger(GetRequestHandler.class.getName());

    public synchronized String handle(Headers headers, BufferedReader in, PrintWriter out) throws IOException {
        readRequestBody(headers, in);

        if(GetRequestHandler.class.getResource("/www"+headers.getPath()) != null) {
            handleFileRequest(headers, out);
        }
        writeResponse(out, HTTP_STATUS_NOT_FOUND, "No data");


        return null;
    }

    private void handleFileRequest(Headers headers, PrintWriter out) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Handling GET request");
        }
        StringBuilder resultStringBuilder = new StringBuilder();
        InputStream fin = GetRequestHandler.class.getResourceAsStream("/www"+headers.getPath());
        try (BufferedReader br = new BufferedReader(new InputStreamReader(fin))) {
            String line;
            while ((line = br.readLine()) != null) {
                resultStringBuilder.append(line).append("\n");
            }
        } catch (IOException e) {
            writeResponse(out, HTTP_STATUS_NOT_FOUND, "No file data");
            LOG.error(e.getMessage(), e);
        }

        writeResponse(out, HTTP_STATUS_OK,  resultStringBuilder.toString());
    }

}
