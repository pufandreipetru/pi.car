package pi.car.socket.server.handler;

import org.apache.log4j.Logger;
import pi.car.model.Headers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Basic request handler for POST method
 */
public class PostRequestHandler extends BaseRequestHandler {
    private static final Logger LOG = Logger.getLogger(PostRequestHandler.class);
    public String handle(Headers headers, BufferedReader in, PrintWriter out) throws IOException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Handling POST request");
        }
        readRequestBody(headers, in);
        writeResponse(out, HTTP_STATUS_OK, "");
        return requestBody;
    }
}
