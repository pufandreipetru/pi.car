package pi.car.socket.server;

public interface StopableRunnable extends Runnable {
    /**
     * Add the ability to be stopped to a generic Runnable.
     */
    void stop();
}
