package pi.car.socket.server;

import pi.car.controller.L298NController;
import pi.car.json.JsonDecoder;
import pi.car.model.Headers;
import pi.car.model.NetworkDataPackage;
import pi.car.socket.server.handler.BaseRequestHandler;
import pi.car.socket.server.handler.GetRequestHandler;
import pi.car.socket.server.handler.PostRequestHandler;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;


/**
 * Worker thread for the master thread.
 */
public class WorkerRunnable implements Runnable {
    private static final Logger LOG = Logger.getLogger(WorkerRunnable.class.getName());
    private final JsonDecoder jsonDecoder;

    private Socket clientSocket;
    private L298NController l298NController;

    /**
     * Creates a new MasterWorkerRunnable instance.
     *
     * @param clientSocket - the socket to use for synchronizing
     * @param l298NController - synchronizableRunnable object to follow for synchronization
     */
    public WorkerRunnable(Socket clientSocket, L298NController l298NController, JsonDecoder jsonDecoder) {
        this.clientSocket = clientSocket;
        this.jsonDecoder = jsonDecoder;
        this.l298NController = l298NController;
    }

    /**
     * WorkerRunnable thread logic.
     * Follows the syncronizable and sends the relevant data through the websocket.
     */
    public void run() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Starting the request worker.");
        }
        try (InputStream input = clientSocket.getInputStream();
             OutputStream output = clientSocket.getOutputStream()) {

            final BufferedReader in = new BufferedReader(new InputStreamReader(input));
            final PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(output)), true);
            final List<String> headersLines = readHeaders(in);
            final Headers headers = new Headers(headersLines);
            final String method = headers.getMethod();
            final String requestBody;
            switch (method) {
                case "GET":
                    requestBody = new GetRequestHandler().handle(headers, in, out);
                    break;
                case "POST":
                    requestBody = new PostRequestHandler().handle(headers, in, out);
                    break;
                default:
                    requestBody = new BaseRequestHandler().handle(headers, in, out);
            }

            NetworkDataPackage inputDataPackage = convertToNetworkDataPackage(requestBody);
            this.l298NController.sendDataPackage(inputDataPackage);
            in.close();
            out.close();
            this.clientSocket.close();
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
    }



    private List<String> readHeaders(BufferedReader in) throws IOException {
        List<String> headerLines = new ArrayList<>();
        String line;
        while ((line = in.readLine()) != null) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("REQUEST_LINE:" + line);
            }
            headerLines.add(line);
            if (line.isEmpty()) {
                break;
            }
        }

        return headerLines;
    }

    private NetworkDataPackage convertToNetworkDataPackage(String input) throws IOException {
        if (input != null) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Converting to JSON " + input);
            }
            NetworkDataPackage dataPackage = jsonDecoder.decode(input);
            return dataPackage;
        } else  {
            return new NetworkDataPackage();
        }
    }
}