pi.car
------
**Description**
This projects aims to create a controller for a RaspberryPi controlled car.
The projects is created using java and javaScript.
It is composed of two parts a controller based on L298N chip and a simple http socket build over
Java Sockets which serves and web interface and listens for commands.

The projects is deployed on a Raspberian image containing *motion* package for video streaming.
The Raspberian image is preloaded with the required packages and the pi.car executable jar.

![Front view](gallery/20200329_210259.jpg?raw=true "Front view")

---

## Build and deploy

---

#### In order to use this project you will need.

1. Download the preloaded Raspberian image. [Download](https://drive.google.com/file/d/17OgzbDpqcg5qYkKwgrp0rEA3s5yWrJgJ/view?usp=sharing)
2. Write the image to an sd card (Note that you will need at least a 5GB sd card)
3. Insert the sd card into the RaspberryPi making sure you have a wireless dongle inseted into the usb if the pi you are using does not have integrated wireless adapter.
4. Connect the RaspberryPi according to the schematic you can find in this repo.
5. Start the RaspberryPi
6. The RaspberryPi will act as a wireless access point so search for **PiCar** wireless network and connect to it.
7. Use the password **picar!002**
8. Access in your browser http://192.168.4.1:9090/index.html
10. Enjoy your new toy car

---

#### Access the RaspberryPi via ssh

1. Install **putty** or open a **terminal** if you are using linux
2. For terminal type ssh pi@192.168.4.1 and for putty use **192.168.4.1** as host and **pi** as user
3. Password: **picar!002**

---

#### In order to deploying the code please use the following steps

1. For build only use **mvn clean package** 
2. For build and deploy use **./scripts/deploy.sh**





## Build the car

In order to build the car you will need:

1. 4 x DC motors with gear reduction
2. 1 x Motor driver L298N or something similar
3. 1 x RaspberryPi
4. 1 x 5V Phone battery bank
5. 1 x RaspberryPi camera or USB camera
